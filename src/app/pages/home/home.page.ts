import { Component, ElementRef, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { AnilistService } from 'src/app/providers/anilist/anilist.service';
import { KitsuService } from 'src/app/providers/kitsu/kitsu.service';
import { Anime } from 'src/app/interfaces/anime';
import { ImportUser } from 'src/app/interfaces/import-user';
import { UserService } from 'src/app/providers/users/user.service';
import { Type } from 'src/app/interfaces/type';
import { UserAnime } from 'src/app/interfaces/user-anime';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements AfterViewInit {
  @ViewChildren('barChart') barChart: QueryList<ElementRef>;

  animes: Anime[] = [];
  userAnimes: UserAnime[] = [];
  users: ImportUser[] = [];

  constructor(private userService: UserService, private anilistService: AnilistService, private kitsuService: KitsuService) {
  }

  ngAfterViewInit() {
    // get all users from a json and retrieve their list
    this.userService.getUsers()
      .subscribe(res => {
        // for each item found, push user
        res.forEach(r => this.users.push(r));
        // for each user, get their anime and filter them to retrieve only the same animes
        this.forEachPromise(this.users, this.getAnimesFromUser).then(() => this.getSameAnimes());
    });

    this.barChart.changes.subscribe(c => c.forEach(item => this.createBarChart(item)));
  }

  getAnimeOnKitsu(user: ImportUser): Promise<number> {
    return this.kitsuService.getAnimes(user).then(animes => this.userAnimes.push(({user: user.name, animes}) as UserAnime));
  }

  getAnimeOnAniList(user: ImportUser): Promise<number> {
    return this.anilistService.getAnimes(user).then(animes => this.userAnimes.push(({user: user.name, animes}) as UserAnime));
  }

  getAnimeOnSheet(user: ImportUser) {
    console.error(`SHEET SERVICE NOT YET IMPLEMENTED`);
  }

  getAnimeOnMyAnimeList(user: ImportUser) {
    console.error(`MAL SERVICE NOT YET IMPLEMENTED`);
  }

  getAnimesFromUser(context: HomePage, user: ImportUser): Promise<number> {
    switch (user.type) {
      case Type.ANILIST.toString():
        return context.getAnimeOnAniList(user);
      case Type.KITSU.toString():
        return context.getAnimeOnKitsu(user);
      default:
        console.error(`type error: ${user.type}`);
    }
  }

  getSameAnimes() {
    let i = 0;
    this.userAnimes.forEach(userAnime => {
      userAnime.animes.forEach(currentAnime => {
        if (this.animes.filter(anime =>
          this.removeSpecialCharacters(currentAnime.name) === this.removeSpecialCharacters(anime.name)).length > 0) {
          this.animes.filter(anime =>
            this.removeSpecialCharacters(currentAnime.name) === this.removeSpecialCharacters(anime.name))
              .forEach(anime => anime.users.push(currentAnime.currentUser));
        } else {
          currentAnime.users = [currentAnime.currentUser];
          currentAnime.index = ++i;
          this.animes.push(currentAnime);
        }
      });
    });
    this.emptyNonDuplicateAnimes();
    this.orderByName();
  }

  orderByName() {
    this.animes.sort((a, b) => a.name.localeCompare(b.name));
  }

  emptyNonDuplicateAnimes() {
    this.animes = this.animes.filter(anime => anime.users.length > 1);
  }

  removeSpecialCharacters(str): string {
    return str.replace(/[\P{L}]/giu, '').toLowerCase();
  }

createBarChart(item) {
  const animes = this.animes.filter(anime => anime.index.toString() === item.nativeElement.getAttribute('id'))[0];
  new Chart(item.nativeElement, {
    type: 'horizontalBar',
    data: {
      labels: animes.users.map(user => user.name),
      datasets: [{
        data: animes.users.map(user => user.progress),
        backgroundColor: ['rgb(38, 194, 129, 0.2)', 'rgba(54, 162, 235, 0.2)'],
        borderColor: ['rgba(38, 194, 129, 0.2)', 'rgba(54, 162, 235, 0.2)'],
        borderWidth: 1,
        barPercentage: 1
      }]
    },
    options: {
      legend: {
        display: false
    },
      scales: {
        xAxes: [{
          ticks: {
            max: animes.maxEpisodes == null ? Math.max(... animes.users.map(user => user.progress)) : animes.maxEpisodes,
            beginAtZero: true
          }
        }]
      }
    }
  });
}

  forEachPromise(items, fn) {
    const context = this;
    return items.reduce((promise, item) => {
        return promise.then(() => {
            return fn(context, item);
        });
    }, Promise.resolve());
}
}
