import { User } from './user';

export interface Anime {
    name: string;
    nameJp: string;
    image: string;
    users: User[];
    currentUser: User;
    maxEpisodes: number;
    index: number;
}
