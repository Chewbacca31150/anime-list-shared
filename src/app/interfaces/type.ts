export enum Type {
    ANILIST = 'ANILIST',
    KITSU = 'KITSU',
    MYANIMELIST = 'MYANIMELIST',
    SHEETS = 'SHEETS'
}
