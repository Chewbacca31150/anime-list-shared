export interface User {
    name: string;
    progress: number;
}
