import { Type } from './type';

export interface ImportUser {
    id: string;
    name: string;
    type: Type;
}
