import { ImportUser } from 'src/app/interfaces/import-user';
import { Anime } from 'src/app/interfaces/anime';
import { Injectable } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { AnimeProvider } from 'src/app/interfaces/anime-provider';

@Injectable({
  providedIn: 'root'
})
export class AnilistService implements AnimeProvider {

  constructor() { }

  getAnimes(user: ImportUser): Promise<Anime[]> {
    const query = `query($userId:Int,$userName:String,$type:MediaType){MediaListCollection(userId:$userId,userName:$userName,type:$type){lists{name isCustomList isCompletedList:isSplitCompletedList entries{...mediaListEntry}}user{id name avatar{large}mediaListOptions{scoreFormat rowOrder animeList{sectionOrder customLists splitCompletedSectionByFormat theme}mangaList{sectionOrder customLists splitCompletedSectionByFormat theme}}}}}fragment mediaListEntry on MediaList{id mediaId status score progress progressVolumes repeat priority private hiddenFromStatusLists customLists advancedScores notes updatedAt startedAt{year month day}completedAt{year month day}media{id title{userPreferred romaji english native}coverImage{extraLarge large}type format status episodes volumes chapters averageScore popularity isAdult countryOfOrigin genres bannerImage startDate{year month day}}}`;
    const variables = `{"userId":${user.id},"type":"ANIME"}`;
    const url = 'https://graphql.anilist.co',
    options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
        body: JSON.stringify({ query, variables })
    };
    return fetch(url, options)
      .then(res => res.json())
      .then(res => res.data.MediaListCollection.lists
        .filter(list => list.name === 'Watching')
        .flatMap(list => list.entries)
        .map(entry => ({
          name: entry.media.title.romaji,
          nameJp: entry.media.title.native,
          maxEpisodes: entry.media.episodes,
          image: entry.media.coverImage.large,
          currentUser: ({name: user.name, progress: entry.progress}) as User
        }) as Anime))
      .catch(res => console.error(res));
  }
}
