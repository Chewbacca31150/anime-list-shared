import { TestBed } from '@angular/core/testing';

import { AnilistService } from './anilist.service';

describe('AnilistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnilistService = TestBed.get(AnilistService);
    expect(service).toBeTruthy();
  });
});
