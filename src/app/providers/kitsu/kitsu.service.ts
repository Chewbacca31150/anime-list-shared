import { Injectable } from '@angular/core';
import { Anime } from 'src/app/interfaces/anime';
import { User } from 'src/app/interfaces/user';
import { ImportUser } from 'src/app/interfaces/import-user';
import { AnimeProvider } from 'src/app/interfaces/anime-provider';

@Injectable({
  providedIn: 'root'
})
export class KitsuService implements AnimeProvider {

  constructor() { }

  getAnimes(user: ImportUser): Promise<any> {
    const url = `https://kitsu.io/api/edge/library-entries?fields%5Banime%5D=slug%2CposterImage%2CcanonicalTitle%2Ctitles%2Csynopsis%2Csubtype%2CstartDate%2Cstatus%2CaverageRating%2CpopularityRank%2CratingRank%2CepisodeCount&fields%5Busers%5D=id&filter%5Buser_id%5D=${user.id}&filter%5Bkind%5D=anime&filter%5Bstatus%5D=current&include=anime%2Cuser%2CmediaReaction&page%5Boffset%5D=0&page%5Blimit%5D=40&sort=-progressed_at`,
    options = {
      method: 'GET',
      headers: {
          'Content-Type': 'application/vnd.api+json',
          Accept: 'application/vnd.api+json',
      }
  };
    return fetch(url, options)
    .then(res => res.json())
    .then(entry =>
      entry.included
      .filter(included => included.type === 'anime')
      .map(included =>
        ({
        name: included.attributes.titles.en_jp,
        nameJp: included.attributes.titles.ja_jp,
        image: included.attributes.posterImage.large,
        maxEpisodes: included.attributes.episodeCount,
        currentUser: ({
          name: user.name,
          progress: entry.data.filter(ent => ent.relationships.anime.data.id === included.id)[0].attributes.progress
        }) as User
      }) as Anime))
    .catch(res => console.error(res));
  }
}
