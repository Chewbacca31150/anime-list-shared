import { Anime } from './anime';

export interface UserAnime {
    user: string;
    animes: Anime[];
}
