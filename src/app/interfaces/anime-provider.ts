import { ImportUser } from './import-user';
import { Anime } from './anime';

export interface AnimeProvider {
    getAnimes(user: ImportUser): Promise<Anime[]>;
}
